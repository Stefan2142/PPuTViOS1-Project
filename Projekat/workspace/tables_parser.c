#include "tables.h"

ParseErrorCode parsePatHeader(const uint8_t* patHeaderBuffer, PatHeader* patHeader)
{    
    if (patHeaderBuffer==NULL || patHeader==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }

    patHeader->tableId = (uint8_t)* patHeaderBuffer; 
    if (patHeader->tableId != 0x00)
    {
        printf("\n%s : ERROR it is not a PAT Table\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;
    
    lower8Bits = (uint8_t)(*(patHeaderBuffer + 1));
    lower8Bits = lower8Bits >> 7;
    patHeader->sectionSyntaxIndicator = lower8Bits & 0x01;

    higher8Bits = (uint8_t) (*(patHeaderBuffer + 1));
    lower8Bits = (uint8_t) (*(patHeaderBuffer + 2));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    patHeader->sectionLength = all16Bits & 0x0FFF;
    
    higher8Bits = (uint8_t) (*(patHeaderBuffer + 3));
    lower8Bits = (uint8_t) (*(patHeaderBuffer + 4));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    patHeader->transportStreamId = all16Bits & 0xFFFF;
    
    lower8Bits = (uint8_t) (*(patHeaderBuffer + 5));
    lower8Bits = lower8Bits >> 1;
    patHeader->versionNumber = lower8Bits & 0x1F;

    lower8Bits = (uint8_t) (*(patHeaderBuffer + 5));
    patHeader->currentNextIndicator = lower8Bits & 0x01;

    lower8Bits = (uint8_t) (*(patHeaderBuffer + 6));
    patHeader->sectionNumber = lower8Bits & 0xFF;

    lower8Bits = (uint8_t) (*(patHeaderBuffer + 7));
    patHeader->lastSectionNumber = lower8Bits & 0xFF;

    return TABLES_PARSE_OK;
}

ParseErrorCode parsePatServiceInfo(const uint8_t* patServiceInfoBuffer, PatServiceInfo* patServiceInfo)
{
    if (patServiceInfoBuffer==NULL || patServiceInfo==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;

    higher8Bits = (uint8_t) (*(patServiceInfoBuffer));
    lower8Bits = (uint8_t) (*(patServiceInfoBuffer + 1));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    patServiceInfo->programNumber = all16Bits & 0xFFFF; 

    higher8Bits = (uint8_t) (*(patServiceInfoBuffer + 2));
    lower8Bits = (uint8_t) (*(patServiceInfoBuffer + 3));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    patServiceInfo->pid = all16Bits & 0x1FFF;
    
    return TABLES_PARSE_OK;
}

ParseErrorCode parsePatTable(const uint8_t* patSectionBuffer, PatTable* patTable)
{
    uint8_t * currentBufferPosition = NULL;
    uint32_t parsedLength = 0;
    
    if (patSectionBuffer==NULL || patTable==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    if (parsePatHeader(patSectionBuffer,&(patTable->patHeader))!=TABLES_PARSE_OK)
    {
        printf("\n%s : ERROR parsing PAT header\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    parsedLength = 12 /*PAT header size*/ - 3 /*Not in section length*/;
    currentBufferPosition = (uint8_t *)(patSectionBuffer + 8); /* Position after last_section_number */
    patTable->serviceInfoCount = 0; /* Number of services info presented in PAT table */
    
    while(parsedLength < patTable->patHeader.sectionLength)
    {
        if (patTable->serviceInfoCount > TABLES_MAX_NUMBER_OF_PIDS_IN_PAT - 1)
        {
            printf("\n%s : ERROR there is not enough space in PAT structure for Service info\n", __FUNCTION__);
            return TABLES_PARSE_ERROR;
        }
        
        if (parsePatServiceInfo(currentBufferPosition, &(patTable->patServiceInfoArray[patTable->serviceInfoCount])) == TABLES_PARSE_OK)
        {
            currentBufferPosition += 4; /* Size from program_number to pid */
            parsedLength += 4; /* Size from program_number to pid */
            patTable->serviceInfoCount ++;
        }    
    }
    
    return TABLES_PARSE_OK;
}


ParseErrorCode parseEitTable(const uint8_t* eitElementaryBuffer, EitTable* eitTable)
{
    uint8_t * currentBufferPosition = NULL;
    uint32_t parsedLength = 0;
    
    if (eitElementaryBuffer==NULL || eitTable==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    if (parseEitHeader(eitElementaryBuffer,&(eitTable->eitTableHeader))!=TABLES_PARSE_OK)
    {
        printf("\n%s : ERROR parsing PAT header\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    parsedLength = 14 /*EIT header size*/ + 4 /*CRC size*/ - 3;
    currentBufferPosition = (uint8_t *)(eitElementaryBuffer + 14); /* Position after last_section_number */
    eitTable->elementaryInfoCount=0;
    
    while(parsedLength < eitTable->eitTableHeader.sectionLength)
    {
        if (eitTable->elementaryInfoCount > TABLES_MAX_NUMBER_OF_PIDS_IN_EIT - 1)
        {
            printf("\n%s : ERROR there is not enough space in PAT structure for Service info\n", __FUNCTION__);
            return TABLES_PARSE_ERROR;
        }
        
        if (parseEitElementaryInfo(currentBufferPosition, &(eitTable->eitElementaryInfoArray[eitTable->elementaryInfoCount])) == TABLES_PARSE_OK)
        {
            currentBufferPosition += 12 + eitTable->eitElementaryInfoArray[eitTable->elementaryInfoCount].descriptorsLoopLength; /* Size from program_number to pid */
            parsedLength += 12 + eitTable->eitElementaryInfoArray[eitTable->elementaryInfoCount].descriptorsLoopLength; /* Size from program_number to pid */
            eitTable->elementaryInfoCount ++;
        }    
    }
    
    return TABLES_PARSE_OK;
}

ParseErrorCode printPatTable(PatTable* patTable)
{
    uint8_t i=0;
    
    if (patTable==NULL)
    {
        printf("\n%s : ERROR received parameter is not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    printf("\n********************PAT TABLE SECTION********************\n");
    printf("table_id                 |      %d\n",patTable->patHeader.tableId);
    printf("section_length           |      %d\n",patTable->patHeader.sectionLength);
    printf("transport_stream_id      |      %d\n",patTable->patHeader.transportStreamId);
    printf("section_number           |      %d\n",patTable->patHeader.sectionNumber);
    printf("last_section_number      |      %d\n",patTable->patHeader.lastSectionNumber);
    
    for (i=0; i<patTable->serviceInfoCount;i++)
    {
        printf("-----------------------------------------\n");
        printf("program_number           |      %d\n",patTable->patServiceInfoArray[i].programNumber);
        printf("pid                      |      %d\n",patTable->patServiceInfoArray[i].pid); 
    }
    printf("\n********************PAT TABLE SECTION********************\n");
    
    return TABLES_PARSE_OK;
}


ParseErrorCode parsePmtHeader(const uint8_t* pmtHeaderBuffer, PmtTableHeader* pmtHeader)
{

    if (pmtHeaderBuffer==NULL || pmtHeader==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }

    pmtHeader->tableId = (uint8_t)* pmtHeaderBuffer; 
    if (pmtHeader->tableId != 0x02)
    {
        printf("\n%s : ERROR it is not a PMT Table\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;

    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 1));
    lower8Bits = lower8Bits >> 7;
    pmtHeader->sectionSyntaxIndicator = lower8Bits & 0x01;
    
    higher8Bits = (uint8_t) (*(pmtHeaderBuffer + 1));
    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 2));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    pmtHeader->sectionLength = all16Bits & 0x0FFF;

    higher8Bits = (uint8_t) (*(pmtHeaderBuffer + 3));
    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 4));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    pmtHeader->programNumber = all16Bits & 0xFFFF;
   // printf("%s \n",pmtHeader->programNumber);

    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 5));
    lower8Bits = lower8Bits >> 1;
    pmtHeader->versionNumber = lower8Bits & 0x1F;

    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 5));
    pmtHeader->currentNextIndicator = lower8Bits & 0x01;

    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 6));
    pmtHeader->sectionNumber = lower8Bits & 0xFF;

    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 7));
    pmtHeader->lastSectionNumber = lower8Bits & 0xFF;

    higher8Bits = (uint8_t) (*(pmtHeaderBuffer + 8));
    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 9));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    pmtHeader->pcrPid = all16Bits & 0xFFFF;

    higher8Bits = (uint8_t) (*(pmtHeaderBuffer + 10));
    lower8Bits = (uint8_t) (*(pmtHeaderBuffer + 11));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    pmtHeader->programInfoLength = all16Bits & 0x0FFF;

    return TABLES_PARSE_OK;
}


// sekcija iz for-a iz dokumentacije
ParseErrorCode parsePmtElementaryInfo(const uint8_t* pmtElementaryInfoBuffer, PmtElementaryInfo* pmtElementaryInfo)
{
    if (pmtElementaryInfoBuffer==NULL || pmtElementaryInfo==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;
    // TODO: implement
    pmtElementaryInfo->streamType = *(pmtElementaryInfoBuffer);	

    higher8Bits = (uint8_t) (*(pmtElementaryInfoBuffer + 1));	//3(reserved) + 5(PID)
    lower8Bits = (uint8_t) (*(pmtElementaryInfoBuffer + 2));	//8(PID)
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    pmtElementaryInfo->elementaryPid = all16Bits & 0x1FFF;

  	higher8Bits = (uint8_t) (*(pmtElementaryInfoBuffer + 3)); //4(reserved) + 4(ES_info_length)
    lower8Bits = (uint8_t) (*(pmtElementaryInfoBuffer + 4));  //8(ES_info_length)
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
	pmtElementaryInfo->esInfoLength = all16Bits & 0x0FFF;     

    return TABLES_PARSE_OK;
}


ParseErrorCode parsePmtTable(const uint8_t* pmtSectionBuffer, PmtTable* pmtTable)
{
    uint8_t * currentBufferPosition = NULL;
    uint32_t parsedLength = 0;
    
    if (pmtSectionBuffer==NULL || pmtTable==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    if (parsePmtHeader(pmtSectionBuffer,&(pmtTable->pmtHeader))!=TABLES_PARSE_OK)
    {
        printf("\n%s : ERROR parsing PMT header\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    

    parsedLength = 12 + pmtTable->pmtHeader.programInfoLength /*PMT header size*/ + 4 /*CRC size*/ - 3 /*Not in section length*/;
    currentBufferPosition = (uint8_t *)(pmtSectionBuffer + 12 + pmtTable->pmtHeader.programInfoLength); /* Position after last descriptor */
    pmtTable->elementaryInfoCount = 0; /* Number of elementary info presented in PMT table */
    

    // Until the same field doesn't arrive
    while(parsedLength < pmtTable->pmtHeader.sectionLength)
    {
        if (pmtTable->elementaryInfoCount > TABLES_MAX_NUMBER_OF_ELEMENTARY_PID - 1)
        {
            printf("\n%s : ERROR there is not enough space in PMT structure for elementary info\n", __FUNCTION__);
            return TABLES_PARSE_ERROR;
        }
        
        if (parsePmtElementaryInfo(currentBufferPosition, &(pmtTable->pmtElementaryInfoArray[pmtTable->elementaryInfoCount])) == TABLES_PARSE_OK)
        {
            currentBufferPosition += 5 + pmtTable->pmtElementaryInfoArray[pmtTable->elementaryInfoCount].esInfoLength; /* Size from stream type to elemntary info descriptor*/
            parsedLength += 5 + pmtTable->pmtElementaryInfoArray[pmtTable->elementaryInfoCount].esInfoLength; /* Size from stream type to elementary info descriptor */
            pmtTable->elementaryInfoCount++;
        }    
    }
  	 // printPmtTable(pmtTable);

    return TABLES_PARSE_OK;
}

ParseErrorCode printPmtTable(PmtTable* pmtTable)
{
    uint8_t i=0;
    
    if (pmtTable==NULL)
    {
        printf("\n%s : ERROR received parameter is not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    printf("\n********************PMT TABLE SECTION********************\n");
    printf("table_id                 |      %d\n",pmtTable->pmtHeader.tableId);
    printf("section_length           |      %d\n",pmtTable->pmtHeader.sectionLength);
    printf("program_number           |      %d\n",pmtTable->pmtHeader.programNumber);
    printf("section_number           |      %d\n",pmtTable->pmtHeader.sectionNumber);
    printf("last_section_number      |      %d\n",pmtTable->pmtHeader.lastSectionNumber);
    printf("program_info_legth       |      %d\n",pmtTable->pmtHeader.programInfoLength);
    
    for (i=0; i<pmtTable->elementaryInfoCount;i++)
    {
        printf("-----------------------------------------\n");
        printf("stream_type              |      %d\n",pmtTable->pmtElementaryInfoArray[i].streamType);
        printf("elementary_pid           |      %d\n",pmtTable->pmtElementaryInfoArray[i].elementaryPid);
    }
    printf("\n********************PMT TABLE SECTION********************\n");
    
    return TABLES_PARSE_OK;
}


ParseErrorCode parseEitHeader(const uint8_t* eitHeaderBuffer, EitTableHeader* eitTableHeader)
{    
    if (eitHeaderBuffer==NULL || eitTableHeader==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }

    eitTableHeader->tableId = (uint8_t)* eitHeaderBuffer; 
    if (eitTableHeader->tableId != 0x4E)
    {
        printf("\n%s : ERROR it is not a PAT Table\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;
    
    lower8Bits = (uint8_t)(*(eitHeaderBuffer + 1));
    lower8Bits = lower8Bits >> 7;
    eitTableHeader->sectionSyntaxIndicator = lower8Bits & 0x01;
	
    lower8Bits = (uint8_t)(*(eitHeaderBuffer + 1));
    lower8Bits = lower8Bits >> 6;
    eitTableHeader->reservedFutureUse = lower8Bits & 0x01;

    lower8Bits = (uint8_t)(*(eitHeaderBuffer + 1));
    lower8Bits =  lower8Bits >> 4;
    eitTableHeader->reserved1 = lower8Bits & 0x03;
    
    higher8Bits = (uint8_t) (*(eitHeaderBuffer + 1));
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 2));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    eitTableHeader->sectionLength = all16Bits & 0x0FFF;
    
    higher8Bits = (uint8_t) (*(eitHeaderBuffer + 3));
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 4));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    eitTableHeader->serviceId = all16Bits;
    
    lower8Bits = (uint8_t)(*(eitHeaderBuffer + 5));
    lower8Bits = lower8Bits >> 6;
    eitTableHeader->reserved2 = lower8Bits & 0x03;
    
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 5));
    lower8Bits = lower8Bits >> 1;
    eitTableHeader->versionNumber = lower8Bits & 0x1F;

    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 5));
    eitTableHeader->currentNextIndicator = lower8Bits & 0x01;

    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 6));
    eitTableHeader->sectionNumber = lower8Bits & 0xFF;

    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 7));
    eitTableHeader->lastSectionNumber = lower8Bits;
    
    higher8Bits = (uint8_t) (*(eitHeaderBuffer + 8));
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 9));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    eitTableHeader->transportStreamId = all16Bits;
    
    higher8Bits = (uint8_t) (*(eitHeaderBuffer + 10));
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 11));
    all16Bits = (uint16_t) ((higher8Bits << 8) + lower8Bits);
    eitTableHeader->originalNetworkId = all16Bits;
    
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 12));
    eitTableHeader->segmentLastSectionNumber = lower8Bits;
    
    lower8Bits = (uint8_t) (*(eitHeaderBuffer + 13));
    eitTableHeader->lastTableId = lower8Bits;

    return TABLES_PARSE_OK;
}

ParseErrorCode parseEitElementaryInfo(const uint8_t* eitElementaryInfoBuffer, EitElementaryInfo* eitElementaryInfo)
{
    if (eitElementaryInfoBuffer==NULL || eitElementaryInfo==NULL)
    {
        printf("\n%s : ERROR received parameters are not ok\n", __FUNCTION__);
        return TABLES_PARSE_ERROR;
    }
    
    int i;
    int m=0;
    
    uint8_t lower8Bits = 0;
    uint8_t higher8Bits = 0;
    uint16_t all16Bits = 0;
    
    uint8_t first = 0;
    uint8_t second = 0;
    uint8_t third = 0;
    uint8_t fourth = 0;
    uint8_t fifth = 0;
    uint64_t startTimeBuffer = 0;
    uint32_t durationBuffer = 0;
    
    // TODO: implement

    higher8Bits = *(eitElementaryInfoBuffer);
    lower8Bits = *(eitElementaryInfoBuffer+1);
    all16Bits = (uint16_t)((higher8Bits << 8) + lower8Bits);
    eitElementaryInfo->eventId = all16Bits;
    
    first = *(eitElementaryInfoBuffer+2);
    second = *(eitElementaryInfoBuffer+3);
    third = *(eitElementaryInfoBuffer+4);
    fourth = *(eitElementaryInfoBuffer+5);
    fifth = *(eitElementaryInfoBuffer+6);
      
    eitElementaryInfo->hours=third;
    eitElementaryInfo->minutes=fourth;
    eitElementaryInfo->seconds=fifth;
    
    startTimeBuffer = ((uint64_t)first << 32) + (second << 24) + (third << 16) + (fourth << 8) + fifth;
    eitElementaryInfo->startTime = startTimeBuffer;
  
   // printf("StartTime %02x:%02x:%02x\n",third,fourth,fifth);
    
    first = (uint8_t)(*(eitElementaryInfoBuffer + 7));
    second = (uint8_t)(*(eitElementaryInfoBuffer + 8));
    third = (uint8_t)(*(eitElementaryInfoBuffer + 9));
    durationBuffer = (uint32_t)((first << 16) + (second << 8) + third);
    eitElementaryInfo->duration = durationBuffer;

    lower8Bits = (uint8_t)(*(eitElementaryInfoBuffer + 10));
    lower8Bits = lower8Bits >> 5;
    eitElementaryInfo->runningStatus = lower8Bits & 0x07;

    lower8Bits = (uint8_t)(*(eitElementaryInfoBuffer + 10));
    lower8Bits = lower8Bits >> 4;
    eitElementaryInfo->freeCAmode = lower8Bits & 0x01;

    higher8Bits = (uint8_t)(*(eitElementaryInfoBuffer + 10));
    lower8Bits = (uint8_t)(*(eitElementaryInfoBuffer + 11));
    all16Bits = (uint16_t)((higher8Bits << 8) + lower8Bits);
    eitElementaryInfo->descriptorsLoopLength = all16Bits & 0x0FFF;

    //parsing descriptor
	if (eitElementaryInfo->runningStatus == 0x4)
    {
		while(m < eitElementaryInfo->descriptorsLoopLength)
		{
			eitElementaryInfo->shortEventDescriptor.descriptorTag = *(eitElementaryInfoBuffer + 12 + m);
			eitElementaryInfo->shortEventDescriptor.descriptorLength = *(eitElementaryInfoBuffer + 12 + 1 + m);	
			if (eitElementaryInfo->shortEventDescriptor.descriptorTag == 0x4d)
			{
				eitElementaryInfo->shortEventDescriptor.eventNameLength = *(eitElementaryInfoBuffer + 12 + 5 + m);
				for(i = 0; i < eitElementaryInfo->shortEventDescriptor.eventNameLength; i++)
				{
					eitElementaryInfo->shortEventDescriptor.eventNameChar[i] = (char)(*(eitElementaryInfoBuffer + 12 + 6 + i + m + 1));
				}
				eitElementaryInfo->shortEventDescriptor.eventNameChar[eitElementaryInfo->shortEventDescriptor.eventNameLength - 1] = '\0';
			}
			
			m += eitElementaryInfo->shortEventDescriptor.descriptorLength + 2;
		}
	}
		
    return TABLES_PARSE_OK;
}

